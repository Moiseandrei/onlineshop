package com.online.shop.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class ContactUs {

    @Id
    @GeneratedValue
    private String contactUsId;


    @ManyToOne
    @JoinColumn
    private Contact contact;
}
