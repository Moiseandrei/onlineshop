package com.online.shop.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;


@Entity
@Getter
@Setter
public class Contact {

    @Id
    @GeneratedValue
    private Integer id;
    private String yourName;
    private String email;
    private String subject;

    @OneToMany(mappedBy = "contact", cascade = CascadeType.ALL)
  private List<ContactUs> contactUsList;

}
