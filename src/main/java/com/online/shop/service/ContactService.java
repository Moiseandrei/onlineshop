package com.online.shop.service;

import com.online.shop.dto.ContactDto;
import com.online.shop.entities.Contact;
import com.online.shop.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContactService {

    @Autowired
    private ContactRepository contactRepository;

    public ContactDto getContactDtoByEmail(String loggedInUserEmail) {
        Optional<Contact> optionalContact = Optional.ofNullable(contactRepository.findByEmail(loggedInUserEmail));
        Contact contact = contactRepository.findByEmail(loggedInUserEmail);

        return new ContactDto();

    }
}
