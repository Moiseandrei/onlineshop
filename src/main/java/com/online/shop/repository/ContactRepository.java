package com.online.shop.repository;

import com.online.shop.entities.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactRepository extends JpaRepository<Contact, Integer> {
    Contact findByEmail(String email);
}
